package pages;

import wrappers.Annotations;

public class CreateLeadPage extends Annotations{
	
	public CreateLeadPage enterFirstName(String data) {
		driver.findElementById("createLeadForm_firstName").sendKeys(data);
		return this;
	}

	public CreateLeadPage enterLastName(String data) {
		driver.findElementById("createLeadForm_lastName").sendKeys(data);
		return this;
	}
	
	public CreateLeadPage enterCompanyname(String data) {
		driver.findElementById("createLeadForm_companyName").sendKeys(data);
		return this;
	}
	
	public ViewLeadPage clickLeadRegister() {
		driver.findElementByXPath("//input[@value='Create Lead']").click();
		return new ViewLeadPage();
	}
}
