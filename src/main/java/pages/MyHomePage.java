package pages;

import wrappers.Annotations;

public class MyHomePage extends Annotations {

	public MyHomePage verifyMyHomepage() {
		String verifyname = driver.findElementByLinkText("My Home").getText();
		if(verifyname.equals("My Home")) {
			System.out.println("Reached to My Home page");
		}else {
			System.out.println("Not Reached to My Home page");
		}
			return this;	
			}
	
	 public MyLeadsPage clickLeadsButton() {
		 driver.findElementByXPath("//a[text()='Leads']").click();
		 return new MyLeadsPage();
	 }
		
	}

