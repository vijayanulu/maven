package pages;

import wrappers.Annotations;

public class MyLeadsPage extends Annotations{

	public MyLeadsPage verifyMyLeadname() {
		String vname= driver.findElementByXPath("//div[text()='My Leads']").getText();
		if(vname.equals("My Leads")) {
			System.out.println("My Leads page name is verified");
		}else {
			System.out.println("My Leads page name is not verified");
		}
		return this;
	}
	
	public CreateLeadPage clickCreateLeadButton() {
		driver.findElementByXPath("//a[text()='Create Lead']").click();
		return new CreateLeadPage();
	}
}
