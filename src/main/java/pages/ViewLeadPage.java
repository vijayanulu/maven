package pages;

import wrappers.Annotations;

public class ViewLeadPage extends Annotations {
	
	public ViewLeadPage viewleadPagename() {
		String vlp=driver.findElementByXPath("//div[text()='View Lead']").getText();
		if(vlp.equals("View Lead")) {
			System.out.println("View lead is successful");
		}else {
			System.out.println("View lead is not successful");
		}
		return this;
	}
      
}
